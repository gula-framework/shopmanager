<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '12fdca734467cbc9a24b9ff6e0a35c69a5f5144f',
        'name' => 'gula/shopmanager',
        'dev' => true,
    ),
    'versions' => array(
        'gregwar/cache' => array(
            'pretty_version' => 'v1.0.13',
            'version' => '1.0.13.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../gregwar/cache/Gregwar/Cache',
            'aliases' => array(),
            'reference' => '184cc341c25298ff7d584f86b55b6ca26626da4f',
            'dev_requirement' => false,
        ),
        'gregwar/image' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../gregwar/image/Gregwar/Image',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => '410a4a21bb1382fb68410edf904b4c54c1942c83',
            'dev_requirement' => false,
        ),
        'gula/shopmanager' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '12fdca734467cbc9a24b9ff6e0a35c69a5f5144f',
            'dev_requirement' => false,
        ),
    ),
);
