<?php

/**
 * @param string $path
 * @return array
 */
function getFolder(string $path , string $requestedPath): array
{
    setupStorage();
    $subfolders = [];

    $structure['home'] = getStructure(storage_path() . $path);

    $files = scandir(storage_path() . $requestedPath);

    foreach ($files as $key => $file) {

        if (is_dir(storage_path() . $requestedPath . '/' . $file) || $file === '.' || $file === '..') {
            if ($file !== '.' && $file !== '..') {
                $subfolders[] = [
                    'path' => '/manage?path=' . $path . '/' . $file,
                    'name' => $file
                ];
            }
            unset($files[$key]);
        } else {
            $files[$key] = str_replace('/cdn/', '', $requestedPath . '/' . $file);
        }
    }

    $rPath = explode('/', $requestedPath);

    return [
        'structure' => $structure,
        'structureHtml' => structureHtml($structure, $requestedPath),
        'files' => $files,
        'subfolders' => $subfolders,
        'breadCrumbs' => breadCrumbs($requestedPath),
        'currentFolder' => end($rPath),
    ];
}

/**
 * @param string $dir
 * @return array
 */
function getStructure(string $dir): array
{
    $fileInfo = scandir($dir);
    $allFileLists = [];

    foreach ($fileInfo as $folder) {
        if ($folder !== '.' && $folder !== '..') {
            if (is_dir($dir . DIRECTORY_SEPARATOR . $folder) === true) {
                $allFileLists[$folder] = getStructure($dir . DIRECTORY_SEPARATOR . $folder);
            } else {
//                $allFileLists[$folder] = $folder;
            }
        }
    }

    return $allFileLists;
}

/**
 * @param array $structure
 * @param $requestedPath
 * @return string
 */
function structureHtml(array $structure, $requestedPath): string
{
    return getSubStructure($structure, $requestedPath, '', '');
}

/**
 * @param array $structure
 * @param string $requestedPath
 * @param string $path
 * @param string $offset
 * @return string
 */
function getSubStructure(array $structure, string $requestedPath, string $path = '', string $offset = ''): string
{
    $html = '';

    foreach ($structure as $key => $folder) {
        $rKey = $key === 'home' ? 'cdn' : $key;
        $folderName = '/' . $path . $rKey === $requestedPath ? '<b>' . $key . '</b>' : $key;
        $html .= <<<HTML
<a href="/manage?path=/{$path}{$rKey}">{$offset}{$folderName}</a><br/>
HTML;
        if (is_array($folder)) {
            $path = $path . $rKey . '/';
            $offset .= '&nbsp;&nbsp;&nbsp;&nbsp;';
            $html .= getSubStructure($folder, $requestedPath, $path, $offset);
            $offset = substr($offset, -24);
            $path = str_replace($rKey . '/', '', $path);
        }

    }

    return $html;
}

/**
 * @param string $path
 * @return string
 */
function breadCrumbs(string $path): string
{
    $path = str_replace(storage_path(), '', $path);
    $path = substr($path, 1);
    $breadCrumbs = '';
    $uri = '/manage?path=';

    $pArray = explode('/', $path);

    foreach ($pArray as $key => $item) {
        $uri .= '/' . $item;
        $item = str_replace('cdn', ' home', $item);
        $nextCrumb = $key !== array_key_last($pArray) ? '&nbsp;&nbsp;&raquo;&nbsp;&nbsp;' : '';

        $breadCrumbs .= <<<HTML
<a href="{$uri}">$item</a>{$nextCrumb}
HTML;
    }

    return $breadCrumbs;
}
