<?php

function setupStorage()
{
    $standardFolders = [
        '/cdn',
        '/cdn_cache',
        '/cdn_trash'
    ];

    foreach ($standardFolders as $folder){
        @mkdir(storage_path() . $folder);
    }
}
