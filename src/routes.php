<?php
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'web', 'namespace' => 'Gula\Shopmanager\Controllers'], function()
{
    Route::get('/shopmanager/products', ['uses' => 'Product@index']);
    Route::get('/shopmanager/product/{action}', ['uses' => 'Product@action']);
    Route::get('/shopmanager/product/{action}/{id}', ['uses' => 'Product@action']);
    Route::post('/shopmanager/product', ['uses' => 'Product@save']);

    Route::get('/shopmanager/categories', ['uses' => 'Category@index']);
    Route::get('/shopmanager/category/{action}/{id}', ['uses' => 'Category@action']);
    Route::get('/shopmanager/category/{action}', ['uses' => 'Category@action']);
    Route::post('/shopmanager/category', ['uses' => 'Category@save']);

    Route::get('/shopmanager/suppliers', ['uses' => 'Supplier@index']);

    Route::get('/shopmanager/page/{action}/{pageSlug}', ['uses' => 'Page@action']);
    Route::post('/shopmanager/page', ['uses' => 'Page@save']);

    Route::get('/resizer/{path}', ['uses' => 'Cdn@index'])->where('path', '.+');

    Route::get('/shopmanager/upload', ['uses' => 'Upload@index']);
    Route::get('/shopmanager/filemanager', ['uses' => 'FileManager@index']);

    Route::get('/shopmanager', ['uses' => 'Shopmanager@index']);
});

//Route::get('shopmanager', function(){
//    echo 'Hello from the shopmanager package!';
//});

