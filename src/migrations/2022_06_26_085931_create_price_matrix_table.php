<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceMatrixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_matrix', function (Blueprint $table) {
            $table->id();
            $table->integer('id_product');
            $table->integer('amount');
            $table->integer('gross_price_cents');
            $table->timestamps();
            $table->index(['id_product', 'amount'], 'default_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_matrix');
    }
}
