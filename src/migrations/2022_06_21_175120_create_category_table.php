<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 *
 */
class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->id();
            $table->integer('id_supplier')->nullable(false);
            $table->integer('id_parent_category')->nullable(true)->default(null);
            $table->string('slug')->nullable(true);
            $table->string('name')->nullable(true);
            $table->string('description');
            $table->string('meta_title');
            $table->string('meta_description');
            $table->boolean('active')->default(true);
            $table->boolean('deleted')->default(false);
            $table->timestamps();
            $table->index(['id', 'id_supplier', 'id_parent_category', 'name', 'slug', 'deleted', 'active'], 'basic_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
