<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_templates', function (Blueprint $table) {
            $table->id();
            $table->integer('id_domain');
            $table->string('image_template_name');
            $table->integer('width')->nullable(false)->default(300);
            $table->integer('height')->nullable(false)->default(300);
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_templates');
    }
}
