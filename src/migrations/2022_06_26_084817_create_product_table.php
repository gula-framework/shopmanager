<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->integer('id_supplier')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->integer('id_category')->nullable(false)->default(0);
            $table->text('name')->nullable(false)->default('nieuw product');
            $table->text('description');
            $table->text('description_excerpt');
            $table->integer('id_tax')->nullable(false)->default(2);
            $table->boolean('active')->default(true);
            $table->boolean('deleted')->default(false);
            $table->text('meta_title');
            $table->text('meta_description');
            $table->timestamps();
            $table->index(['id', 'id_supplier', 'slug', 'id_category', 'active', 'deleted'],'default_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
