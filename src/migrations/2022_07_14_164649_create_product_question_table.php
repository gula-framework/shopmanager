<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_question', function (Blueprint $table) {
            $table->id();
            $table->integer('id_product')->nullable(false);
            $table->integer('id_question')->nullable(false);
            $table->string('value')->nullable(false);
            $table->timestamps();
            $table->index(['id_product', 'id_question','id'],'product_question');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_question');
    }
}
