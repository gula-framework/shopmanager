<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionalProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_product', function (Blueprint $table) {
            $table->id();
            $table->integer('id_main_product');
            $table->integer('id_additional_product');
            $table->boolean('deleted')->default(false);
            $table->timestamps();
            $table->index(['id', 'id_main_product', 'id_additional_product'], 'default_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_product');
    }
}
