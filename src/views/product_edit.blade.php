@extends('shopmanager::base')
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"><i class="fas fa-table"></i>
                    Product {!! !empty($product->id) ? $product->name . ' bewerken' : 'toevoegen' !!}</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="/shopmanager/products">Producten</a></li>
                    <li class="breadcrumb-item active">
                </ol>
                <div class="card mb-4">
                    <div class="card-body">
                        <form method="post" autocomplete="off" enctype="multipart/form-data"
                              action="/shopmanager/product">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{!! $product->id !!}">


                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="general-tab" data-bs-toggle="tab" href="#general"
                                       role="tab" aria-controls="general" aria-selected="true">Algemeen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="seo-tab" data-bs-toggle="tab" href="#seo" role="tab"
                                       aria-controls="seo" aria-selected="false">SEO</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="images-tab" data-bs-toggle="tab" href="#images" role="tab"
                                       aria-controls="images" aria-selected="false">Afbeeldingen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="prices-tab" data-bs-toggle="tab" href="#prices" role="tab"
                                       aria-controls="prices" aria-selected="false">Prijzen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="properties-tab" data-bs-toggle="tab"
                                       href="#properties" role="tab"
                                       aria-controls="properties" aria-selected="false">Kenmerken</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <br/>
                                <br/>
                                <div class="tab-pane fade show active" id="general" role="tabpanel"
                                     aria-labelledby="general-tab">
                                    @include('shopmanager::partials.product_general')
                                </div>
                                <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                                    @include('shopmanager::partials.product_seo')
                                </div>
                                <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="seo-tab">
                                    @include('shopmanager::partials.product_images')
                                </div>
                                <div class="tab-pane fade" id="prices" role="tabpanel" aria-labelledby="seo-tab">
                                    @include('shopmanager::partials.product_prices_delivery')
                                </div>
                                <div class="tab-pane fade" id="properties" role="tabpanel" aria-labelledby="seo-tab">
                                    @include('shopmanager::partials.product_properties')
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12 col-lg-3 col-md-3">
                                    <button type="submit" class="btn btn-success col-12">Opslaan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Gula webdesign 2022</div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/datatables-latest.js')}}"></script>
    <script src="{{asset('js/datatables.js')}}"></script>
    <script src="{{asset('js/shopmanager.js')}}"></script>
    <script>
        var properties = [{!! $properties !!}];
        @foreach($productProperties as $key => $property)
            autocomplete(document.getElementById("propertyName_{{$key}}"), properties);
        @endforeach

        var questions = [{!! $specificQuestions !!}];
        @foreach($productQuestions as $key => $question)
            autocomplete(document.getElementById("questionName_{{$key}}"), questions);
        @endforeach
    </script>
@endsection
