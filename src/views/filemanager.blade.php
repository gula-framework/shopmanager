@extends('shopmanager::base')
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"><i class="fas fa-image"></i> Afbeeldingen beheren</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Afbeeldingen</li>
                </ol>
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2 folderstructure">
                                <h3>Mappen</h3>
                                {!!  $structure['structureHtml']!!}
                            </div>

                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">{!! $structure['breadCrumbs'] !!}</div>
                                    </div>
                                </div>
                                <div class="row ßfolderSelection">
                                    <div class="col-2">

                                        <img src="{!! '/resizer/70x70/icons/open_folder.png' !!}" title="Klik om te wijzigen"/>
                                        <b style="text-decoration: 10px" title="Klik om te wijzigen">{{$structure['currentFolder']}}</b>
                                    </div>
                                </div>
                                <div class="row folderSelection">
                                    @foreach($structure['subfolders'] as $subfolder)
                                        <div class="col-1">
                                            <a href="{{$subfolder['path']}}" style="text-decoration: none">
                                                <img src="{!! '/resizer/70x70/icons/folder.png' !!}"/>
                                                {{$subfolder['name']}}
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row files">
                                    @foreach($structure['files'] as $image)
                                        <div class="col-2">
                                            <img src="/resizer/180x180/{{$image}}" />
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Gula webdesign 2022</div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/datatables-latest.js')}}"></script>
    <script src="{{asset('js/datatables.js')}}"></script>
@endsection
