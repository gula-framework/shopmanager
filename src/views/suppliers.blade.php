@extends('shopmanager::base')
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"><i class="fa-solid fa-boxes-packing"></i></i> Leveranciers</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Leveranciers</li>
                </ol>
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4">
                            <div class="card-body">Primary Card</div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="#">View Details</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-warning text-white mb-4">
                            <div class="card-body">Warning Card</div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="#">View Details</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-success text-white mb-4">
                            <div class="card-body">Success Card</div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="#">View Details</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-danger text-white mb-4">
                            <div class="card-body">Danger Card</div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="#">View Details</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-4">
{{--                    <div class="card-header">--}}
{{--                        <i class="fas fa-table me-1"></i>--}}
{{--                        DataTable Example--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                            <tr>
                                <th>Naam</th>
                                <th>e-mail</th>
                                <th>phone number</th>
                                <th>Dropshipment</th>
                                <th>Xml</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Naam</th>
                                <th>e-mail</th>
                                <th>phone number</th>
                                <th>Dropshipment</th>
                                <th>Xml</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($suppliers as $supplier)
                            <tr>
                                <td>{!! $supplier->name !!}</td>
                                <td>{!! $supplier->email !!}</td>
                                <td>{!! $supplier->phone_number !!}</td>
                                <td>{!! $supplier->drop_shipment === true ? 'Ja' : 'Nee' !!}</td>
                                <td>{!! $supplier->xml === true ? 'Ja' : 'Nee' !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Gula webdesign 2022</div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/datatables-latest.js')}}"></script>
    <script src="{{asset('js/datatables.js')}}"></script>
@endsection
