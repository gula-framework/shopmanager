@extends('shopmanager::base')
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"><i class="fas fa-book-open"></i>
                    Pagina {!! !empty($pageSlug) ? $pageSlug . ' bewerken' : 'toevoegen' !!}</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item">Pagina's</li>
                    <li class="breadcrumb-item active">
                        Pagina {!! !empty($pageSlug) ? $pageSlug . ' bewerken' : 'toevoegen' !!}</li>
                </ol>
                <div class="card mb-4">
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" action="/shopmanager/page">
                            {{csrf_field()}}
                            <input type="hidden" name="page" value="{!! $pageSlug !!}">
                            @foreach($page as $blockKey => $block)
                                <h3>Banner {!! ucfirst($blockKey) !!}</h3>
                            <br/>
                                @foreach($block as $itemKey => $item)
                                    <h5>Blok {!! ucfirst($itemKey) !!}</h5>
                                <small>Afmetingen: {{$item['width']}} x {{$item['height']}} px</small>

                                    <div class="row">
                                        <div class="form-group col-12 col-lg-6 col-md-6">
                                            <label for="url">Afbeelding url:</label>
                                            <input type="text" class="form-control" name="{{$pageSlug}}[{{$blockKey}}][{{$itemKey}}][image]" value="{!! $item['image'] !!}"
                                                   placeholder="/resizer/{{$item['width']}}x{{$item['height']}}/crop=100,290/pollepels/golfset.jpg" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-12 col-lg-6 col-md-6">
                                            <label for="url">Link naar:</label>
                                            <input type="text" class="form-control" name="{{$pageSlug}}[{{$blockKey}}][{{$itemKey}}][url]" value="{!! $item['url'] !!}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-12 col-lg-6 col-md-6">
                                            <label for="alt">Alt-tekst:</label>
                                            <input type="text" class="form-control" name="{{$pageSlug}}[{{$blockKey}}][{{$itemKey}}][alt]" value="{!! $item['alt'] !!}">
                                        </div>
                                    </div>
                                    <br/>
                                @endforeach
                            @endforeach




                            <div class="row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-success col-12">Opslaan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Gula webdesign 2022</div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/datatables-latest.js')}}"></script>
    <script src="{{asset('js/datatables.js')}}"></script>
@endsection
