@extends('shopmanager::base')
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"><i class="fas fa-table"></i> Producten</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Producten</li>
                </ol>
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4">
                            <div class="card-body">Mappen producten</div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="/shopmanager/products-mapping">Start koppelen</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="/shopmanager/product/add"><button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Product toevoegen</button> </a>
                    <br/>
                    <br/>
                    <br/>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                            <tr>
                                <th>Naam</th>
                                <th>Categorie</th>
                                <th>Leverancier</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Naam</th>
                                <th>Categorie</th>
                                <th>Leverancier</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td>{!! $product->name !!}</td>
                                <td>{!! $product->category_name !!}</td>
                                <td>{!! $product->supplier_name !!}</td>
                                <td><a href="/shopmanager/product/edit/{{$product->id}}"><i class="fa-solid fa-pen-to-square" title="Wijzigen of verwijderen"></i></a> </td>
                            </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Gula webdesign 2022</div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/datatables-latest.js')}}"></script>
    <script src="{{asset('js/datatables.js')}}"></script>
@endsection
