<div class="row">
    <h2>SEO zoekmachine</h2>
    <br/>
    <br/>
    <br/>
</div>
<div class="row">
    <div class="form-group col-12 col-lg-6 col-md-6">
        <label for="email">Meta title</label>
        <input type="text" class="form-control" name="meta_title"
               value="{!! $product->meta_title !!}">
        <br/>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <label for="id_team">Meta description</label>
        <textarea name="meta_description" class="form-control"
                  rows="2">{!! $product->meta_description !!}</textarea>
        <br/>
    </div>
</div>
