<div class="row">
    <h2>Prijzen & levering</h2>
    <br/>
    <br/>
    <br/>
</div>
<div class="row">
    <div class="col-12 col-lg-4">
        <label for="purchase_price_cents">Inkoopprijs &euro;</label>
        <input type="number" name="purchase_price_cents" step=".05" class="form-control"
               value="{{$product->purchase_price_cents / 100 }}">
        <br/>
    </div>
</div>
<div class="row">
    <div class="col-6 col-lg-4">
        <label for="shipment_in_cents">Verzendkosten &euro;</label>
        <input type="number" name="shipment_in_cents" step=".05" class="form-control"
               value="{{$product->shipment_in_cents / 100 }}">
        <br/>
    </div>
    <div class="col-6 col-lg-4">
        <label for="collected_shipment">Verzendkosten hoort bij hele bestelling?</label>
        <select name="collected_shipment" class="form-control">
            <option value="1" @if($product->collected_shipment === 1) selected @endif>Ja
            </option>
            <option value="0" @if($product->collected_shipment === 0) selected @endif>Nee
            </option>
        </select>
        <br/>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <label for="pick_up">Product mag afgehaald worden?</label>
        <select name="pick_up" class="form-control">
            <option value="1" @if($product->pick_up === 1) selected @endif>Ja</option>
            <option value="0" @if($product->pick_up === 0) selected @endif>Nee</option>
        </select>
        <br/>
    </div>

</div>
<div class="row">
    <div class="col-6">
        <label for="name">Btw percentage:</label>
        <select name="id_tax" class="form-control">
            @foreach($taxes as $tax)
                <option value="{{$tax->id}}"
                        @if($product->id_tax === $tax->id) selected @endif>{{$tax->name}}
                </option>
            @endforeach
        </select>
        <br/>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Aantal</th>
                <th>Prijs</th>
            </tr>
            </thead>
            <tbody>
            @foreach($priceSet as $key => $price)
                <tr>
                    <td><input type="number" name="price[{{$key}}][amount]" step="1"
                               value="{{$price['amount']}}"></td>
                    <td><input type="number" name="price[{{$key}}][price]" step="0.05"
                               value="{{$price['gross_price_cents'] / 100 }}" min="0"
                               class="form-control">
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
