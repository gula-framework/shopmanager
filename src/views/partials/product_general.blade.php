<div class="row">
    <h2>Algemeen</h2>
    <br/>
    <br/>
    <br/>
</div>
<div class="row">
    <div class="form-group col-12 col-lg-6 col-md-6">
        <label for="email">Naam*</label>
        <input type="text" class="form-control" name="name" value="{!! $product->name !!}"
               required>
        <br/>
    </div>
</div>
<div class="row">
    <div class="form-group col-12 col-lg-6 col-md-6">
        <label for="id_team">Categorie</label>
        <select class="form-control" name="id_category" required>
            <option></option>
            @foreach($tree as $key=> $leaf)
                <option value="{{$key}}"
                        @if($product->id_category === $key)selected @endif>{{$leaf}}</option>
            @endforeach
        </select>
        <br/>
    </div>
</div>
<div class="row">
    <div class="form-group col-12 col-lg-6 col-md-6">
        <label for="id_team">Leverancier</label>
        <select class="form-control" name="id_supplier" required>
            <option></option>
            @foreach($suppliers as $supplier)
                <option value="{{$supplier->id}}"
                        @if($product->id_supplier === $supplier->id)selected @endif>{{$supplier->name}}</option>
            @endforeach
        </select>
        <br/>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <label for="id_team">Omschrijving</label>
        <textarea name="description" class="form-control"
                  rows="6">{!! $product->description !!}</textarea>
        <br/>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <label for="id_team">Korte omschrijving (overzichtspagina's)</label>
        <textarea name="description_excerpt" class="form-control"
                  rows="3">{!! $product->description_excerpt !!}</textarea>
        <br/>
    </div>
</div>
