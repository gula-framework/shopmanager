<div class="row">
    <h2>Eigenschappen</h2>
    <br/>
    <br/>
    <br/>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Kenmerk</th>
                <th>Waarde</th>
            </tr>
            </thead>
            <tbody>
            @foreach($productProperties as $key => $property)
                <tr >
                    <td class="autocomplete"><input type="text" id="propertyName_{{$key}}" name="properties[{{$key}}][name]" value="{{$property['name']}}"></td>
                    <td><input type="text" name="properties[{{$key}}][value]" value="{{$property['value']}}"
                               class="form-control">
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div
<hr/>
<div class="row">
    <h2>Vragen bij bestellen</h2>
    <br/>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Vraag</th>
            </tr>
            </thead>
            <tbody>
            @foreach($productQuestions as $key => $question)
                <tr >
                    <td class="autocomplete"><input type="text" id="questionName_{{$key}}" name="questions[{{$key}}][name]" value="{{$question['name']}}"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
