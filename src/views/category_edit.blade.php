@extends('shopmanager::base')
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"><i class="fas fa-list"></i> Categorie {!! !empty($category->id) ? $category->name . ' bewerken' : 'toevoegen' !!}</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="/shopmanager/categories">Categorieën</a></li>
                    <li class="breadcrumb-item active">
                        Categorie {!! !empty($category->id) ? $category->name . ' bewerken' : 'toevoegen' !!}</li>
                </ol>
                <div class="card mb-4">
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" action="/shopmanager/category">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{!! $category->id !!}">
                            <div class="row">
                                <div class="form-group col-12 col-lg-6 col-md-6">
                                    <label for="email">Naam*</label>
                                    <input type="text" class="form-control" name="name" value="{!! $category->name !!}"
                                           required>
                                    <br/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-lg-6 col-md-6">
                                    <label for="id_team">Bovenliggende categorie</label>
                                    <select class="form-control" name="id_parent_category">
                                        <option></option>
                                        @foreach($tree as $key=> $leaf)
                                            <option value="{{$key}}"
                                                    @if($category->id_parent_category === $key)selected @endif>{{$leaf}}</option>
                                        @endforeach
                                    </select>
                                    <br/>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-lg-6 col-md-6">
                                    <label for="id_team">Leverancier</label>
                                    <select class="form-control" name="id_supplier">
                                        <option></option>
                                        @foreach($suppliers as $supplier)
                                            <option value="{{$supplier->id}}"
                                                    @if($category->id_supplier === $supplier->id)selected @endif>{{$supplier->name}}</option>
                                        @endforeach
                                    </select>
                                    <br/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label for="id_team">Omschrijving</label>
                                    <textarea name="description" class="form-control"
                                              rows="10">{!! $category->description !!}</textarea>
                                    <br/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-lg-6 col-md-6">
                                    <label for="email">Meta title</label>
                                    <input type="text" class="form-control" name="meta_title" value="{!! $category->meta_title !!}">
                                    <br/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label for="id_team">Meta description</label>
                                    <textarea name="meta_description" class="form-control"
                                              rows="2">{!! $category->meta_description !!}</textarea>
                                    <br/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-success col-12">Opslaan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Gula webdesign 2022</div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/datatables-latest.js')}}"></script>
    <script src="{{asset('js/datatables.js')}}"></script>
@endsection
