@extends('shopmanager::base')
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"><i class="fas fa-list"></i> Categorieën</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Categorieën</li>
                </ol>
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4">
                            <div class="card-body">Mappen categorieën</div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="/shopmanager/categories-mapping">Start koppelen</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="/shopmanager/category/add"><button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Categorie toevoegen</button> </a>
                    <br/>
                    <br/>
                    <br/>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                            <tr>
                                <th>Naam</th>
                                <th>Leverancier</th>
                                <th>Bovenliggende categorie</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Naam</th>
                                <th>Leverancier</th>
                                <th>Bovenliggende categorie</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>{!! $category->name !!}</td>
                                <td>{!! $category->supplier_name !!}</td>
                                <td>{!! $category->parent_category_name !!}</td>
                                <td><a href="/shopmanager/category/edit/{{$category->id}}"><i class="fa-solid fa-pen-to-square" title="Wijzigen of verwijderen"></i></a> </td>
                            </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Gula webdesign 2022</div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/datatables-latest.js')}}"></script>
    <script src="{{asset('js/datatables.js')}}"></script>
@endsection
