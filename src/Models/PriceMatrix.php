<?php

namespace Gula\Shopmanager\Models;

use Illuminate\Support\Facades\DB;

/**
 * @property integer $id
 * @property integer $id_product
 * @property integer $amount
 * @property integer $gross_price_cents
 * @property boolean $deleted
 */
class PriceMatrix extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'price_matrix';
    protected $guarded = [];

    /**
     * @param int|null $idProduct
     * @return array
     */
    public function getPriceSet(?int $idProduct): array
    {
        $priceSet = [];
        $priceSetCounter = 0;

        if (true === empty($idProduct)) {
            for ($i = 1; $i < 11; $i++) {
                $priceSet[] = [
                    'amount' => 0,
                    'gross_price_cents' => 0
                ];
            }

            $priceSet[0] = [
                'amount' => 1,
                'gross_price_cents' => 0];
            return $priceSet;
        }

        $priceMatrix = $this->getPriceMatrix($idProduct);

        foreach ($priceMatrix as $price) {
            if ($price->gross_price_cents > 0 || $price->amount > 0) {
                $priceSetCounter++;
                $priceSet[] = [
                    'amount' => $price->amount,
                    'gross_price_cents' => $price->gross_price_cents
                ];
            }
        }

        for ($i = $priceSetCounter; $i < 10; $i++) {
            $priceSet[] = [
                'amount' => 0,
                'gross_price_cents' => 0
            ];
        }

        return $priceSet;
    }

    /**
     * @param int $idProduct
     * @return array
     */
    public function getPriceMatrix(int $idProduct): array
    {
        return DB::table($this->table)
            ->where('id_product', '=', $idProduct)
            ->where('deleted', '=', false)
            ->orderBy('amount', 'asc')
            ->get()
            ->toArray();
    }
}
