<?php

namespace Gula\Shopmanager\Models;

/**
 * @property int $id
 * @property int $percentage
 * @property string $name
 */
class Tax extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'taxes';
    protected $guarded = [];

}
