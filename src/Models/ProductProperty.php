<?php

namespace Gula\Shopmanager\Models;

/**
 * @property int $id
 * @property int $id_property
 * @property int $id_product
 * @property string $value
 *
 */
class ProductProperty extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'product_property';
    protected $guarded = [];

    /**
     * @param int $idProduct
     * @return array
     */
    public function getProductProperties(int $idProduct): array
    {
        $data = [];
        $prodProperties = $this
            ->select('product_property.*', 'properties.name as property_name')
            ->leftJoin('properties', 'properties.id', '=', 'product_property.id_property')
            ->where('product_property.id_product', '=', $idProduct)
            ->orderBy('product_property.id', 'asc')
            ->get();

        foreach ($prodProperties as $property) {
            $data[] = [
                'id' => $property->id,
                'name' => $property->property_name,
                'value' => $property->value
            ];

        }
        for ($x = 0; $x < 5; $x++) {
            $data[] = ['id' => null, 'name' => '', 'value' => ''];
        }

        return $data;
    }
}
