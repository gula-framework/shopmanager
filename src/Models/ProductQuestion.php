<?php

namespace Gula\Shopmanager\Models;

/**
 * @property int $id
 * @property int $id_question
 * @property int $id_product
 */
class ProductQuestion extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'product_question';
    protected $guarded = [];

    /**
     * @param int $idProduct
     * @return array
     */
    public function getSpecificationQuestions(int $idProduct): array
    {
        $data = [];
        $prodspecification_question = $this
            ->select('product_question.*', 'specification_question.name as question_name')
            ->leftJoin('specification_question', 'specification_question.id', '=', 'product_question.id_question')
            ->where('product_question.id_product', '=', $idProduct)
            ->orderBy('product_question.id', 'asc')
            ->get();

        foreach ($prodspecification_question as $question) {
            $data[] = [
                'id' => $question->id,
                'name' => $question->question_name,
            ];

        }
        for ($x = 0; $x < 5; $x++) {
            $data[] = ['id' => null, 'name' => ''];
        }

        return $data;
    }

}
