<?php

namespace Gula\Shopmanager\Models;

/**
 * @property int $id
 * @property string $name
 */
class Property extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'properties';
    protected $guarded = [];

    /**
     * @return string
     */
    public function getPropertiesAsString(): string
    {
        $propertiesString = '';
        $properties = $this->orderBy('name', 'asc')->get();
        foreach ($properties as $property){
//            $propertiesString.= ',' . '"' . $property->name . ' [' . $property->id .']"';
            $propertiesString.= ',' . '"' . $property->name . '"';
        }

        return substr($propertiesString,1);
    }

    /**
     * @param string $propertyName
     * @return int
     */
    public function getId(string $propertyName): int
    {
        $property = $this->where('name', '=', $propertyName)->first();

        if(true === empty($property))
        {
            $property = new $this;
            $property->name = $propertyName;
            $property->save();
        }

        return $property->id;
    }
}
