<?php

namespace Gula\Shopmanager\Models;

/**
 * @property int $id
 * @property string $name
 * @property string $information
 */
class SpecificationQuestion extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'specification_question';
    protected $guarded = [];

    /**
     * @return string
     */
    public function getQuestionsAsString(): string
    {
        $questionString = '';
        $questions = $this->orderBy('name', 'asc')->get();
        foreach ($questions as $question){
            $questionString.= ',' . '"' . $question->name . '"';
        }

        return substr($questionString,1);
    }

    /**
     * @param string $questionName
     * @return int
     */
    public function getId(string $questionName): int
    {
        $question = $this->where('name', '=', $questionName)->first();

        if(true === empty($question))
        {
            $question = new $this;
            $question->name = $questionName;
            $question->save();
        }

        return $question->id;
    }
}
