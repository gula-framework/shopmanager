<?php

namespace Gula\Shopmanager\Models;

/**
 * @property int $id
 * @property string $page
 * @property string $section
 * @property string $parameters
 */
class Page extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'page';
    protected $guarded = [];

    public function getBannerTemplates():array
    {
        return [
            'home' => [
                'area' => [
                    'left' => [
                        'width' => 960,
                        'height' => 850,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ],
                    'right_top' => [
                        'width' => 960,
                        'height' => 424,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ],
                    'right_bottom_left' => [
                        'width' => 481,
                        'height' => 427,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ],
                    'right_bottom_right' => [
                        'width' => 326,
                        'height' => 229,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ],
                ],
                'area2' => [
                    'left' => [
                        'width' => 326,
                        'height' => 608,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ],
                    'middle_top' => [
                        'width' => 681,
                        'height' => 361,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ],
                    'middle_bottom_left' => [
                        'width' => 326,
                        'height' => 229,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ],
                    'middle_bottom_right' => [
                        'width' => 326,
                        'height' => 229,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ],
                    'right' => [
                        'width' => 326,
                        'height' => 608,
                        'url' =>'',
                        'image' => '',
                        'alt' => '',
                    ]
                ],
            ]
        ];
    }
}
