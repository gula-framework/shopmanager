<?php

namespace Gula\Shopmanager\Models;

use Gregwar\Image\Image;

class ImageResizer extends \Illuminate\Database\Eloquent\Model
{
    private $location = '';
    private $imageNotFound = '';
//    private $cachePath = '../storage/cdn/cache/';
    private $imagePath;
    private $width;
    private $height;
    private $file;
    private $status;
    private $content;

    public function __construct(array $attributes = [])
    {
        $this->location = storage_path() . '/cdn';
        $this->imageNotFound = $this->location . '/images/notfound.jpg';
    }

    public function getHttpHeaders()
    {
        if ($this->status == 'HTTP/1.1 200') {
            $lastModified = filemtime($this->file);
            $etagFile = md5_file($this->file);
        } else {
            $lastModified = filemtime($this->imageNotFound);
            $etagFile = md5_file($this->imageNotFound);
        }

        return array('Cache-Control:public,max-age=31536000', "Content-Type: " . (file_exists($this->file) ? mime_content_type($this->file) : 'image/png'), "Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModified), "Etag: $etagFile");
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setContent($path, $save = false, $cacheUrl = null)
    {
        $request = explode("/", $path);
        $size = explode("x", $request[0]);
        $this->width = $size[0];
        $this->height = $size[1];

        for ($x = 1; $x <= count($request) - 2; $x++) {
            if (false === str_contains($request[$x], 'crop')) {
                $this->imagePath .= "/" . $request[$x];
            } else {
                $crop = explode(',', str_replace('crop=', '', $request[$x]));
            }
        }

        $this->file = $this->location . $this->imagePath . '/' . $request[count($request) - 1];

        $this->status = 'HTTP/1.1 200';

        if ($this->width <= 3000 && $this->height <= 2000) {
            try {
                if (false === empty($crop)) {
                    $image = Image::open($this->file);
                    $newImage = $image
                        ->crop($crop[0], $crop[1], $image->width() - $crop[0], $image->height() - $crop[1])
                        ->zoomCrop($this->width, $this->height, 'transparent', 'left', 'top');

                    $cache = $newImage->save($cacheUrl);
                    $this->content = $newImage->get();
                } else {
                    $this->content = Image::open($this->file)
                        ->resize($this->width, $this->height)
                        ->get();
                }

                if (!file_exists($this->file)) {
                    $this->content = Image::open($this->imageNotFound)
                        ->resize($this->width, $this->height)
                        ->get();

                    $this->status = 'HTTP/1.1 404';
                }

            } catch (\Exception $e) {

            }
        }
    }
}
