<?php

namespace Gula\Shopmanager\Models;

/**
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone_number
 * @property boolean $drop_shipment
 * @property boolean $xml
 * @property boolean $active
 * @property boolean $deleted
 *
 */
class Supplier extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'supplier';
    protected $guarded = [];
}
