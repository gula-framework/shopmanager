<?php

namespace Gula\Shopmanager\Models;

/**
 * @property integer $id
 * @property integer $id_supplier
 * @property integer $id_category
 * @property string $name
 * @property string $description
 * @property string $description_excerpt
 * @property string $meta_title
 * @property string $meta_description
 * @property integer $id_tax
 * @property integer $shipment_in_cents
 * @property boolean $pick_up
 * @property boolean $collected_shipment
 * @property integer $purchase_price_cents
 * @property boolean $active
 * @property boolean $deleted
 */
class Product extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'product';
    protected $guarded = [];

}
