<?php

namespace Gula\Shopmanager\Models;

class ImageTemplate extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'image_templates';
    protected $guarded = [];

}
