<?php

namespace Gula\Shopmanager\Models;

/**
 * @property integer $id
 * @property string $entity
 * @property integer $id_entity
 * @property boolean $deleted
 */
class Image extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'images';
    protected $guarded = [];

}
