<?php

namespace Gula\Shopmanager\Models;

use Illuminate\Support\Facades\DB;

/**
 * @property integer $id
 * @property integer $id_supplier
 * @property integer $id_parent_category
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property boolean $active
 * @property boolean $deleted
 *
 */
class Category extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'category';
    protected $guarded = [];

    /**
     * @return array
     */
    public function getTree(): array
    {
        $structure = [];

        $categories = $this
            ->orderBy('name', 'asc')
            ->get();

        foreach ($categories as $category){
            $structure[$category->id] = $category->id_parent_category . $category->name;
        }

        return $structure;
    }
}
