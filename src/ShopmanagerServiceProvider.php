<?php

namespace Gula\Shopmanager;

use Illuminate\Support\ServiceProvider;

class ShopmanagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(__DIR__ .'/Helpers/*.php') as $filename){
            require_once($filename);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');;
        $this->loadViewsFrom(__DIR__ . '/views', 'shopmanager');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations/');

        $this->publishes([__DIR__ . '/public/css' => public_path('css')], 'public');
        $this->publishes([__DIR__ . '/public/js' => public_path('js')], 'public');
        $this->publishes([__DIR__ . '/public/dropzone' => public_path('dropzone')], 'public');

        $this->copyIcons();
    }

    private function copyIcons()
    {
        @mkdir(storage_path() . 'icons');
        foreach (glob(__DIR__ . '/storage/icons/*.*') as $filename){
            $dFileName = str_replace(__DIR__ . '/storage/icons/', storage_path() .'/icons/', $filename);
            $a= __DIR__ . '/storage/icons/';
            @copy($filename, $dFileName);
        }
    }
}
