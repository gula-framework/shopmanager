<?php

namespace Gula\Shopmanager\Controllers;

class ImageTemplate extends \App\Http\Controllers\Controller
{
    public function index()
    {
        $imageTemplates = (new \Gula\Shopmanager\Models\ImageTemplate())
            ->orderBy('active', 'DESC')
            ->orderBy('deleted', 'ASC')
            ->orderBy('image_template_name', 'ASC')
            ->get();

        return view('shopmanager::image_templates', compact('imageTemplates'));
    }
}

