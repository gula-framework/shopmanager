<?php

namespace Gula\Shopmanager\Controllers;

use Illuminate\Http\Request;

class Category extends \App\Http\Controllers\Controller
{
    public function index()
    {
        $categories = (new \Gula\Shopmanager\Models\Category())
            ->select('category.*', 'supplier.name as supplier_name', 'category2.name as parent_category_name')
            ->leftJoin('supplier', 'supplier.id', '=', 'category.id_supplier', ['name as supplier'])
            ->leftJoin('category as category2', 'category2.id','=','category.id_parent_category' )
            ->orderBy('category.name')
            ->get();

        return view('shopmanager::categories', compact('categories'));
    }

    public function action(Request $request, string $action, int $id=null)
    {
        $mdlCategory = new \Gula\Shopmanager\Models\Category();

        $suppliers = (new \Gula\Shopmanager\Models\Supplier())
            ->orderBy('name', 'asc')
            ->get();

        $tree = $mdlCategory->getTree();


        switch ($action) {
            case 'add':
                $category = new $mdlCategory;
                break;
            case 'edit':
                $category = (new \Gula\Shopmanager\Models\Category())
                ->where('id','=',$id)
                ->first();
                break;
        }

        return view('shopmanager::category_edit', compact('category', 'suppliers', 'tree'));
    }

    public function save(Request $request)
    {
        $post = $request->post();
        $mdlCategory = new \Gula\Shopmanager\Models\Category();

        if (false === empty($post['id'])) {
            $category = $mdlCategory->where('id', '=', $post['id'])->first();
        } else {
            $category = new $mdlCategory;
            $category->slug = 'placeholder';
        }

        $category->name = $post['name'];
        $category->description = $post['description'];
        $category->meta_title = $post['meta_title'];
        $category->meta_description = $post['meta_description'];
        $category->id_supplier = $post['id_supplier'];
        $category->id_parent_category = $post['id_parent_category'];

        $category->save();

        if($category->slug === 'placeholder'){
            $category->slug = slugify($category->name . '-' . $category->id);
            $category->save();
        }

        return redirect('/shopmanager/categories');
    }
}
