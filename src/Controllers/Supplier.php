<?php

namespace Gula\Shopmanager\Controllers;

class Supplier extends \App\Http\Controllers\Controller
{
    public function index()
    {
        $suppliers = (new \Gula\Shopmanager\Models\Supplier())
            ->orderBy('active', 'DESC')
            ->orderBy('deleted', 'ASC')
            ->orderBy('name', 'ASC')
            ->get();

        return view('shopmanager::suppliers', compact('suppliers'));
    }
}
