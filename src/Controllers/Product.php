<?php

namespace Gula\Shopmanager\Controllers;

use Gula\Shopmanager\Models\PriceMatrix;
use Gula\Shopmanager\Models\ProductProperty;
use Gula\Shopmanager\Models\ProductQuestion;
use Gula\Shopmanager\Models\Property;
use Gula\Shopmanager\Models\SpecificationQuestion;
use Gula\Shopmanager\Models\Tax;
use Illuminate\Http\Request;

class Product extends \App\Http\Controllers\Controller
{
    public function index()
    {
        $products = (new \Gula\Shopmanager\Models\Product())
            ->select('product.*', 'supplier.name as supplier_name', 'category.name as category_name')
            ->leftJoin('supplier', 'supplier.id', '=', 'product.id_supplier', ['name as supplier'])
            ->leftJoin('category as category', 'category.id', '=', 'product.id_category')
            ->orderBy('product.name')
            ->get();

        return view('shopmanager::products', compact('products'));
    }

    public function action(Request $request, string $action, int $id = null)
    {
        $mdlCategory = new \Gula\Shopmanager\Models\Category();
        $mdlProduct = new \Gula\Shopmanager\Models\Product();
        $mdlPriceSet = new PriceMatrix();
        $taxes = (new Tax())->orderBy('percentage', 'asc')->get();
        $properties = (new Property())->getPropertiesAsString();
        $specificQuestions = (new SpecificationQuestion())->getQuestionsAsString();
        $productProperties = (new ProductProperty())->getProductProperties($id);
        $productQuestions = (new ProductQuestion())->getSpecificationQuestions($id);


        $suppliers = (new \Gula\Shopmanager\Models\Supplier())
            ->orderBy('name', 'asc')
            ->get();

        $tree = $mdlCategory->getTree();

        $priceSet = $mdlPriceSet->getPriceSet($id);

        switch ($action) {
            case 'add':
                $product = new $mdlProduct;
                break;
            case 'edit':
                $product = (new \Gula\Shopmanager\Models\Product())
                    ->where('id', '=', $id)
                    ->first();
                break;
        }

        $args = [];
        return view('shopmanager::product_edit', compact('product', 'suppliers', 'tree', 'priceSet', 'taxes', 'properties', 'productProperties', 'specificQuestions', 'productQuestions'));
    }

    public function save(Request $request)
    {
        $post = $request->post();
        $mdlProduct = new \Gula\Shopmanager\Models\Product();
        $mdlPriceSet = new PriceMatrix();
        $mdlProductProperties = new ProductProperty();
        $mdlProperties = new Property();
        $mdlSpecQuestions = new SpecificationQuestion();
        $mdlProductQuestions = new ProductQuestion();

        if (false === empty($post['id'])) {
            $product = $mdlProduct->where('id', '=', $post['id'])->first();
        } else {
            $product = new $mdlProduct;
            $product->slug = 'placeholder';
        }

        $product->name = $post['name'];
        $product->description = $post['description'];
        $product->id_tax = $post['id_tax'];
        $product->description_excerpt = $post['description_excerpt'];
        $product->meta_title = $post['meta_title'];
        $product->meta_description = $post['meta_description'];
        $product->id_supplier = $post['id_supplier'];
        $product->id_category = $post['id_category'];
        $product->purchase_price_cents = ($post['purchase_price_cents'] * 100);
        $product->shipment_in_cents = ($post['shipment_in_cents'] * 100);
        $product->pick_up = $post['pick_up'];
        $product->collected_shipment = $post['collected_shipment'];

        $product->save();

        if ($product->slug === 'placeholder') {
            $product->slug = slugify($product->name . '-' . $product->id);
            $product->save();
        }

        $mdlPriceSet
            ->where('id_product', '=', $product->id)
            ->delete();

        foreach ($post['price'] as $price) {
            $priceSet = new $mdlPriceSet;
            $priceSet->id_product = $product->id;
            $priceSet->amount = $price['amount'];
            $priceSet->gross_price_cents = ($price['price'] * 100);
            $priceSet->save();
        }

        $mdlProductProperties->where('id_product','=', $product->id)->delete();

        foreach ($post['properties'] as $property)
        {
            if(trim($property['name'] !== '' && $property['value'] !== '' && $property['name'] !== null && $property['value'] !== null)){

                $nProperty = new $mdlProductProperties;
                $nProperty->id_property = $mdlProperties->getId($property['name']);
                $nProperty->id_product = $product->id;
                $nProperty->value = $property['value'];
                $nProperty->save();
            }
        }

        $mdlProductQuestions->where('id_product','=', $product->id)->delete();

        foreach ($post['questions'] as $question)
        {
            if(trim($question['name'] !== '' && $question['name'] !== null)){

                $nQuestion = new $mdlProductQuestions;
                $nQuestion->id_question = $mdlSpecQuestions->getId($question['name']);
                $nQuestion->id_product = $product->id;
                $nQuestion->save();
            }
        }

        return redirect('/shopmanager/products');
    }


}
