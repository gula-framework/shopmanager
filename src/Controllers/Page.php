<?php

namespace Gula\Shopmanager\Controllers;

use Illuminate\Http\Request;

class Page extends \App\Http\Controllers\Controller
{
    public function action(Request $request, string $action, string $pageSlug = null)
    {
        $mdlPages = new \Gula\Shopmanager\Models\Page();
        $templates = $mdlPages->getBannerTemplates();
        $pageData = $mdlPages->where('page', '=', $pageSlug)->first();
        $parameters = false === empty($pageData) ? $pageData->parameters : [];
        $page = $this->mergeDataInTemplate(json_decode($parameters,1), $templates['home']);

        return view('shopmanager::page_edit', compact('templates', 'page', 'pageSlug'));
    }

    /**
     * @param array $pageData
     * @param array $template
     * @return array
     */
    private function mergeDataInTemplate(?array $pageData, array $template): array
    {
        foreach ($template as $blockKey => $block){
            foreach ($block as $itemKey => $item){
                foreach ($item as $key => $value){
                    $data[$blockKey][$itemKey][$key] = false === empty($pageData[$blockKey][$itemKey][$key]) ? $pageData[$blockKey][$itemKey][$key] : $value;
                }
            }
        }

        return $data;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(Request $request)
    {
        $post = $request->post();
        $mdlPage = new \Gula\Shopmanager\Models\Page();

        if (false === empty($post['page'])) {
            $page = $mdlPage->where('page', '=', $post['page'])->first();
        }

        if(true === empty($page)){
            $page = new $mdlPage;
        }

        $aPage =  json_decode(((array)$page->parameters)[0],1);

        foreach ($post['home'] as $key => $value){
            $aPage[$key] = $value;
        }

        $page->page = $post['page'];
        $page->parameters = json_encode($aPage);
        $page->save();

        return redirect('/shopmanager');
    }
}
